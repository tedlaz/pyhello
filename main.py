import os


def num2pre(num: str, digits=3) -> str:
    return '0' * max(digits-len(num), 0) + num


def read_new_names(filename: str) -> dict:
    final = {}
    with open(filename) as fil:
        for line in fil.readlines():
            num, *rest = line.split()
            final[num] = ' '.join(rest)
    return final


def create_new_names(dicnames: dict, postfix='mp4') -> dict:
    return {key: f"{key} {val}.{postfix}" for key, val in dicnames.items()}


def get_numbers_filenames(prefix: str, postfix: str) -> list:
    file_dict = []
    for filename in os.listdir("."):
        if (not filename.startswith(prefix)) and (not filename.endswith(postfix)):
            continue
        nam, _ = filename.split(".")
        num = nam[len(prefix):]
        file_dict.append((num2pre(num), filename))
    return file_dict


def run(prefix, postfix, filename) -> None:
    num_fnames = get_numbers_filenames(prefix, postfix)
    new_names = read_new_names(filename)
    filenames = create_new_names(new_names)
    for num, old_name in num_fnames:
        os.rename(old_name, filenames.get(num, old_name))


if __name__ == '__main__':
    run(prefix='lesson', postfix='mp4', filename='index.txt')
